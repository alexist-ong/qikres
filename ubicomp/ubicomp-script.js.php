<?php
if (!defined('UBI_HOST') || !defined('UBI_CUSTOMER_ID') || (!is_multisite() && !defined('UBI_PROPERTY_CODE'))) {
    return;
}

$qikres_host = UBI_HOST;
$auth_token = (isset($_GET['authtoken']) && !empty($_GET['authtoken'])) ? $_GET['authtoken'] : '';
$auth_data = new stdClass();
$pageName = get_post()->post_name;
if (!empty($pageName)) {
    $pageName = urldecode($pageName);
}

//get logged in auth token
if (!empty($auth_token)) {
    try {
        $url = add_query_arg(
                array(
                    'authToken' => $auth_token
                ),
                "$qikres_host/qikstay/guest/obtainLoggedInProfile.action");

        $response = wp_remote_get($url, array(
            'timeout' => 10
        ));
        if (is_wp_error($response)) {
            throw new Exception($response->get_error_message());
        }

        $body = wp_remote_retrieve_body($response);
        $auth_data = json_decode($body);
    } catch (Exception $ex) {
        error_log($ex->getMessage());
    }
}

//get property settings
$default_property_code = is_multisite() ? getSitePropertyCode(get_bloginfo()) : UBI_PROPERTY_CODE;
$property_settings = get_property_settings();

//get the languages list from polylang
$language_list = array();
if (function_exists('pll_languages_list')) {
    $language_list = pll_languages_list();
}

$page_slugs = array(
    'member' => array('member'),
    'login' => array('login'),
    'initregister' => array('initregister'),
    'signup' => array('signup'),
    'forgot-pwd' => array('forgot-pwd'),
    'reserve' => array('reserve'),
    'reservation' => array('reservation'),
    'non-room-reservation' => array('non-room-reservation')
);

$page_permalinks = array();
foreach ($page_slugs as $key => $value) {
    $page_permalink = array();

    $page_slug = $value;
    $en_page = get_page_by_path($key);
    if (!empty($en_page)) {
        $page_permalink['default'] = urldecode(get_permalink($en_page));

        if (function_exists('pll_get_post') && !empty($language_list)) {
            foreach ($language_list as $slug) {
                $other_page_id = pll_get_post($en_page->ID, $slug);
                if (!empty($other_page_id)) {
                    $other_page = get_post($other_page_id);
                    if (!empty($other_page)) {
                        $other_page_name = urldecode($other_page->post_name);
                        if (!in_array($other_page_name, $page_slug)) {
                            $page_slug[] = urldecode($other_page->post_name);
                        }

                        $page_permalink[$slug] = urldecode(get_permalink($other_page));
                    }
                }
            }

            $page_slugs[$key] = $page_slug;
        }
    }
    $page_permalinks[$key] = $page_permalink;
}
?>
<script defer id="qikresjs" src="<?php echo $qikres_host; ?>/wbe/js/qikres.min.js"></script>
<script type="text/javascript">
    var contextPath = "<?php echo get_site_url(); ?>";
    var site_name = "<?php echo get_bloginfo(); ?>";
    var customerID = "<?php echo UBI_CUSTOMER_ID; ?>";
    var default_propertyCode = "<?php echo $default_property_code; ?>";
    var theme = "<?php echo UBI_THEME; ?>";
    var languageCode = "<?php echo getLanguageCode(); ?>";
    var dateFormat = "yy-mm-dd";
    var query_auth_token = "<?php echo $auth_token; ?>";
    var page_permalinks = <?php echo json_encode($page_permalinks); ?>;
    var propertySettingJSON = <?php echo (!empty((array)$property_settings) && !empty($property_settings->propertySettingJSON)) ? $property_settings->propertySettingJSON : '[]'; ?>;
    var lead_hour = 0;    
    
    if(window.propertySettingJSON) {
        for(var i in propertySettingJSON) {
            var setting = propertySettingJSON[i];
            if(setting.key === "RESV_LEAD_TIME") {
                var lead_time = setting.value;
                try {
                    lead_hour = parseInt(lead_time);
                } catch(err) {
                    console.error(err);
                }
                
                break;
            }
        }
    }

    var getAuthToken = function () {
        var auth_token = query_auth_token || sessionStorage.getItem("auth_token");
        if (!auth_token) {
            auth_token = null;
        }

        return auth_token;
    };

    var closepopup = function () {
        jQuery("#subscriptionform .container").hide();
    };
    
    var process_auth_token = function () {
        if (query_auth_token) {
            var guestName = "<?php echo (!empty($auth_data) && !empty($auth_data->firstName)) ? $auth_data->firstName : ''; ?>";
            if (guestName) {
                sessionStorage.setItem("auth_token", query_auth_token);
                sessionStorage.setItem("qikres_guestName", guestName);
                qikres_update_login({
                    guestName: guestName
                });
            }
        } else {
            var session_auth_token = sessionStorage.getItem("auth_token");
            var session_guestName = sessionStorage.getItem("qikres_guestName");
            if (session_auth_token && session_guestName) {
                qikres_update_login({
                    guestName: session_guestName
                });
            }
        }
    };

    var getDefault_CICO_date = function () {
        try {
            var date = new Date();
            if(lead_hour > 0) {
                date.setTime(date.getTime() + (lead_hour * 3600 * 1000));
            }
            var checkin_date = jQuery.datepicker.formatDate(dateFormat, date);

            var checkout_date = jQuery.datepicker.parseDate(dateFormat, checkin_date);
            checkout_date = checkout_date.setDate(checkout_date.getDate() + 1);
            checkout_date = jQuery.datepicker.formatDate(dateFormat, new Date(checkout_date));

            return {
                "checkin_date": checkin_date,
                "checkout_date": checkout_date
            };
        } catch (err) {
            console.error(err);
        }

        return {
            "checkin_date": "",
            "checkout_date": ""
        };
    };

    var data = {
        'customerID': customerID,
        'embedded': true,
        'embeddedContainer': 'membership',
        'loginSelector': '#login',
        'signUpSelector': '#signup',
        'memberPage': page_permalinks['member'][languageCode] || page_permalinks['member']['default'],
        'external': 1,
        'authToken': getAuthToken(),
        'forgotPwdUrl': page_permalinks['forgot-pwd'][languageCode] || page_permalinks['forgot-pwd']['default'],
        'signUpUrl': page_permalinks['signup'][languageCode] || page_permalinks['signup']['default'],
        'loginUrl': page_permalinks['login'][languageCode] || page_permalinks['login']['default']
    };

    if (theme) {
        data['style'] = theme;
    }

    if (languageCode) {
        data['languageCode'] = languageCode;
    }

    var qikresLogin = function () {
        if (typeof Qikres !== "undefined") {
            jQuery("#primary .container").first().removeClass("container").addClass("no-container");
            Qikres.showLoginFrame(data, function () {
                jQuery('#qikres-window').nextAll().hide();
            });
        }
    };

    var qikresSignup = function () {
        if (typeof Qikres !== "undefined") {
            jQuery("#primary .container").first().removeClass("container").addClass("no-container");

            var additionalParam = {};
            if (Qikres.getQueryString("info")) {
                additionalParam.info = Qikres.getQueryString("info");
            }

            Qikres.showSignUpFrame(jQuery.extend({}, data, additionalParam), function () {
                jQuery('#qikres-window').nextAll().hide();
            });
        }
    };

    var qikresPwd = function () {
        if (typeof Qikres !== "undefined") {
            jQuery("#primary .container").first().removeClass("container").addClass("no-container");
            Qikres.showPwdFrame(data, function () {
                jQuery('#qikres-window').nextAll().hide();
            });
        }
    };

    var qikres_loadNonRoomReservation = function () {
        try {
            if (typeof Qikres !== "undefined") {
                var loginSelector = "";
                var signUpSelector = "";

                var qikres_requestProp = sessionStorage.getItem("qikres_requestProp");
                if (qikres_requestProp != null) {
                    var prop = JSON.parse(qikres_requestProp);
                    loginSelector = prop.loginSelector;
                    signUpSelector = prop.signUpSelector;
                }

                var default_CICO = getDefault_CICO_date();
                var checkin_date = "<?php echo (isset($_GET['checkin']) && !empty($_GET['checkin'])) ? $_GET['checkin'] : ''; ?>" || default_CICO.checkin_date;
                var checkout_date = "<?php echo (isset($_GET['checkout']) && !empty($_GET['checkout'])) ? $_GET['checkout'] : ''; ?>" || default_CICO.checkout_date;
                var propertyCode = "<?php echo (isset($_GET['property']) && !empty($_GET['property'])) ? $_GET['property'] : ''; ?>" || default_propertyCode;

                var params = {
                    propertyCode: propertyCode,
                    isNonRoomRequest: true,
                    checkInDate: checkin_date,
                    checkOutDate: checkout_date,
                    customerID: customerID,
                    external: 1,
                    window: "embedded", // embedded | popup
                    embeddedContainer: "booking", // defined if embedded
                    loginSelector: (typeof loginSelector !== 'undefined') ? loginSelector : '',
                    signUpSelector: (typeof signUpSelector !== 'undefined') ? signUpSelector : '',
                    authToken: getAuthToken(),
                    signUpUrl: page_permalinks['signup'][languageCode] || page_permalinks['signup']['default'],
                    forgotPwdUrl: page_permalinks['forgot-pwd'][languageCode] || page_permalinks['forgot-pwd']['default'],
                    loginUrl: page_permalinks['login'][languageCode] || page_permalinks['login']['default']
                };

                if (theme) {
                    params['style'] = theme;
                }

                if (languageCode) {
                    params['languageCode'] = languageCode;
                }

                Qikres.render(params);
                jQuery('iframe.qikFrame').load(function () {
                    jQuery("html, body").animate({scrollTop: jQuery(".qikFrame").offset().top}, 500);
                });
            }
        } catch (err) {
            console.error(err);
        }
    };

    var qikres_loadRoomReservation = function () {
        try {
            if (typeof Qikres !== "undefined") {
                var loginSelector = "";
                var signUpSelector = "";

                var qikres_requestProp = sessionStorage.getItem("qikres_requestProp");
                if (qikres_requestProp != null) {
                    var prop = JSON.parse(qikres_requestProp);
                    loginSelector = prop.loginSelector;
                    signUpSelector = prop.signUpSelector;
                }

                var default_CICO = getDefault_CICO_date();
                var checkin_date = "<?php echo (isset($_GET['checkin']) && !empty($_GET['checkin'])) ? $_GET['checkin'] : ''; ?>" || default_CICO.checkin_date;
                var checkout_date = "<?php echo (isset($_GET['checkout']) && !empty($_GET['checkout'])) ? $_GET['checkout'] : ''; ?>" || default_CICO.checkout_date;
                var propertyCode = "<?php echo (isset($_GET['property']) && !empty($_GET['property'])) ? $_GET['property'] : ''; ?>" || default_propertyCode;

                var params = {
                    propertyCode: propertyCode,
                    checkInDate: checkin_date,
                    checkOutDate: checkout_date,
                    adult: "<?php echo (isset($_GET['adult']) && !empty($_GET['adult'])) ? $_GET['adult'] : '1'; ?>",
                    children: "<?php echo (isset($_GET['child']) && !empty($_GET['child'])) ? $_GET['child'] : '0'; ?>",
                    customerID: customerID,
                    external: 1,
                    window: 'embedded', // embedded | popup
                    embeddedContainer: "booking", // defined if embedded
                    loginSelector: (typeof loginSelector !== 'undefined') ? loginSelector : '',
                    signUpSelector: (typeof signUpSelector !== 'undefined') ? signUpSelector : '',
                    authToken: getAuthToken(),
                    signUpUrl: page_permalinks['signup'][languageCode] || page_permalinks['signup']['default'],
                    forgotPwdUrl: page_permalinks['forgot-pwd'][languageCode] || page_permalinks['forgot-pwd']['default'],
                    loginUrl: page_permalinks['login'][languageCode] || page_permalinks['login']['default']
                };

                var special = "<?php echo (isset($_GET['specialcode']) && !empty($_GET['specialcode'])) ? $_GET['specialcode'] : ''; ?>";
                var productCode = "<?php echo (isset($_GET['productcode']) && !empty($_GET['productcode'])) ? $_GET['productcode'] : ''; ?>";
                if (special) {
                    params.productCode = special;
                    params.isSpecialRate = true;
                } else if (productCode) {
                    params.productCode = productCode;
                }

                if (theme) {
                    params['style'] = theme;
                }

                if (languageCode) {
                    params['languageCode'] = languageCode;
                }

                Qikres.render(params);
                jQuery('iframe.qikFrame').load(function () {
                    jQuery("html, body").animate({scrollTop: jQuery(".qikFrame").offset().top}, 500);
                });
            }
        } catch (err) {
            console.error(err);
        }
    };

    var scrollto = function () {
        jQuery("html, body").animate({
            scrollTop: parseInt(jQuery("#" + data.embeddedContainer).offset().top) - parseInt(jQuery("#cshero-header").height()) - 20
        }, 1000);
    };

    var handle_qikres_message = function (message) {
        frameMsg = message.data;
        if ((typeof message.data) === "string") {
            var msg = message.data;
            try {
                if (msg.indexOf('s') !== msg.length - 1) {
                    frameMsg = JSON.parse(msg);
                }
            } catch (e) {
                if (console) {
                    console.debug('receiveMessage fail JSON parsing ' + msg);
                }
            }
        }

        if (frameMsg.type === "qikres-login") {
            var payload = frameMsg.payload;
            if (typeof qikres_update_login === "function") {
                qikres_update_login(payload);
            }
        }
        if (frameMsg.type === "qikres-logout") {
            var payload = frameMsg.payload;
            if (typeof qikres_update_logout === "function") {
                qikres_update_logout();
            }
        }
    };

    var qikres_toggle_login = function (flag) {
        jQuery(".qikres-login-menu, .qikres-login-button, .qikres-register-menu").toggleClass("loggedin", (flag === true));
        jQuery("body").toggleClass("has-user-loggedin", (flag === true));
    };

    var qikres_create_usertoken = function (username) {
        var result = null;
        if (typeof username === "string" && username.length > 0) {
            var token = username.charAt(0).toUpperCase();
            result = "<span class='user-icon'>" + token + "</span>";
        }
        return result;
    };

    var qikres_update_login = function (respData) {
        if (respData) {
            qikres_toggle_login(true);
            var menuTitle = jQuery(".qikres-login-menu > a");
            if (menuTitle) {
                menuTitle.text(respData.guestName);
                var loggedInToken = qikres_create_usertoken(respData.guestName);
                if (loggedInToken) {
                    menuTitle.prepend(loggedInToken);
                    menuTitle.addClass("has-user-icon");
                }
            }
        }
    };

    var qikres_update_logout = function () {
        qikres_toggle_login(false);
        jQuery(".qikres-login-menu > a").html("");
        location.href = 'login';
    };

    var memberLogout = function () {
        var logoutData = {
            'customerID': customerID,
            'embedded': true,
            'embeddedContainer': 'membership',
            'loginSelector': '#login',
            'signUpSelector': '#signup',
            'memberPage': page_permalinks['member'][languageCode] || page_permalinks['member']['default']
        };

        if (theme) {
            logoutData['style'] = theme;
        }

        if (languageCode) {
            logoutData['languageCode'] = languageCode;
        }

        Qikres.logoutFrame(logoutData);
    };

    var gotoMemberPage = function () {
        location.href = data.memberPage;
    };
    
    var inject_async_qikresjs = function () {
        var deferredObject = jQuery.Deferred();
        
        if(window.Qikres) {
            deferredObject.resolve();
        } else {
            var script = document.createElement("script");
            var first_script = document.getElementsByTagName("script")[0];
            script.src = "<?php echo $qikres_host; ?>/wbe/js/qikres.min.js";
            script.id = "qikresjs";
            script.async = 1;
            script.onload = deferredObject.resolve;
            first_script.parentNode.insertBefore(script, first_script);
        }
        return deferredObject.promise();        
    };

    document.addEventListener("DOMContentLoaded", function (event) {
        process_auth_token();

        try {
            if (window.location.search) {
                jQuery(".pll-parent-menu-item .menu-item.lang-item a").each(function () {
                    this.href = this.href + window.location.search;
                });
            }
        } catch (err) {
            console.error(err);
        }

        window.addEventListener("message", handle_qikres_message);
        jQuery('.qikres-logout-button').attr("onclick", "memberLogout();");

        var pageName = "<?php echo $pageName ?>";
        switch (pageName) {
<?php
foreach ($page_slugs['member'] as $slug) {
    ?>
                case "<?php echo $slug; ?>":
    <?php
}
?>
<?php
foreach ($page_slugs['login'] as $slug) {
    ?>
                case "<?php echo $slug; ?>":
    <?php
}
?>
                qikresLogin();
                break;
<?php
foreach ($page_slugs['initregister'] as $slug) {
    ?>
                case "<?php echo $slug; ?>":
    <?php
}
?>
<?php
foreach ($page_slugs['signup'] as $slug) {
    ?>
                case "<?php echo $slug; ?>":
    <?php
}
?>
                qikresSignup();
                break;
<?php foreach ($page_slugs['forgot-pwd'] as $slug) {
    ?>
                case "<?php echo $slug; ?>":
    <?php
}
?>
                qikresPwd();
                break;
<?php foreach ($page_slugs['reserve'] as $slug) {
    ?>
                case "<?php echo $slug; ?>":
<?php }
?>
<?php
foreach ($page_slugs['reservation'] as $slug) {
    ?>
                case "<?php echo $slug; ?>":
    <?php
}
?>
                qikres_loadRoomReservation();
                break;
<?php foreach ($page_slugs['non-room-reservation'] as $slug) {
    ?>
                case "<?php echo $slug; ?>":
    <?php
}
?>
                qikres_loadNonRoomReservation();
                break;
            default:
                break;
        }

        jQuery(".qikres-cloak").removeClass("qikres-cloak");
    });
</script>
