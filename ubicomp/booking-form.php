<div class="ubicomp-form--container">
    <form class="ubicomp-form" onsubmit="return submitBookingForm(this);">
        <?php
        if (is_multisite()) {
            ?>
            <div class="booking-form select-property">
                <label><?php _e('Property', 'qikres'); ?></label>
                <select name="property"><?php
                    $site_properties = getSiteProperties();
                    foreach ($site_properties as $site_propcode => $site_propname) {
                        ?>
                        <option value="<?php echo $site_propcode; ?>"><?php echo $site_propname; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <?php
        }
        ?> 
        <div class="booking-form checkin">
            <label><?php _e('Check In', 'qikres'); ?></label>
            <input type="text" name="checkin"/>
            <input type="hidden" name="alternate_checkin" />
        </div>
        <div class="booking-form checkout">
            <div class="night">
                <label><?php _e('Night', 'qikres'); ?></label>
                <input type="number" name="night" value="1" min="1" step="1"/>
            </div>
            <div class="out">
                <label><?php _e('Check Out', 'qikres'); ?></label>
                <input type="text" name="checkout"/>
                <input type="hidden" name="alternate_checkout">
            </div>
        </div>
        <?php        
        $max_adult = 4;
        $max_child = 2;
        $property_settings = get_property_settings();
        if (!empty((array)$property_settings) && !empty($property_settings->propertySettingJSON)) {
            $property_settings_json = json_decode($property_settings->propertySettingJSON);
            
            foreach ($property_settings_json as $property_setting) {
                if($property_setting->key == 'MAX_ADULT') {
                    $max_adult = intval($property_setting->value);
                } else if($property_setting->key == 'MAX_CHILD') {
                    $max_child = intval($property_setting->value);
                }
            }
        } 
        $max_adult = ($max_adult > 0)? $max_adult : 4;
        $max_child = ($max_child > 0)? $max_child : 2;
        ?>
        <div class="booking-form person">
            <div class="adult">
                <label><?php _e('Adult', 'qikres'); ?></label>
                <select name="adult">
                    <option value="1" selected="selected">1</option>
                    <?php
                    for ($i = 2; $i <= $max_adult; $i++) {
                    ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php                        
                    }
                    ?>
                </select>
            </div>
            <div class="child">
                <label><?php _e('Child', 'qikres'); ?></label>
                <select name="child">
                    <option value="0" selected="selected">0</option>
                    <?php
                    for ($i = 1; $i <= $max_child; $i++) {
                    ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php                        
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="booking-form promo">
            <label><?php _e('Promo Code', 'qikres'); ?></label>
            <input type="text" name="specialcode" class="uppercase-text"/>
        </div>
        <div class="booking-form bookbtn">
            <button type="submit"><?php _e('BOOK NOW', 'qikres'); ?></button>
            <a class="closebookbtn" style="margin-left: 20px;" href="javascript:void(0);"><?php _e('Close', 'qikres'); ?></a>
        </div>
    </form>
</div>