<script>
    var wp_is_mobile = <?php echo ( wp_is_mobile() ? 'true' : 'false'); ?>;
    var is_multisite = <?php echo ( is_multisite() ? 'true' : 'false'); ?>;
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    function toggleBooking() {
        jQuery(".ubicomp-form--container").toggleClass("open");
        jQuery('.head-nav .menu').slicknav("close");
    }

    document.addEventListener("DOMContentLoaded", function (event) {
        prepareBookingForm();
    });

    function submitBookingForm(form) {
        try {
            var reservation_page = page_permalinks['reservation'] || page_permalinks['reserve'];
            if(!reservation_page) {
                throw "Undetermined reservation page. Please contact web master to fix this issue.";
            }
            
            var selectedLanguageCode = window.languageCode? languageCode : "";            
            var reservation_path = reservation_page[selectedLanguageCode] || reservation_page['default'];

            var propertyCode = "";
            if (is_multisite) {
                propertyCode = jQuery(form["property"]).val();
                if (!propertyCode) {
                    throw "Please select a property.";
                }
            }

            var check_in = jQuery(form["alternate_checkin"]).val();
            var check_out = jQuery(form["alternate_checkout"]).val();
            if (wp_is_mobile) {
                check_in = jQuery(form["checkin"]).val();
                check_out = jQuery(form["checkout"]).val();
            }

            var adult = jQuery(form["adult"]).val();
            var child = jQuery(form["child"]).val();
            var specialCode = jQuery(form["specialcode"]).val();
            var isUpperCase = jQuery(form["specialcode"]).hasClass("uppercase-text");

            var link = reservation_path + '?checkin=' + check_in + '&checkout=' + check_out + '&adult=' + adult + '&child=' + child;
            if (is_multisite) {
                link += ('&property=' + propertyCode);
            }
            if (specialCode !== '' && typeof specialCode !== "undefined") {
                if (isUpperCase) {
                    specialCode = specialCode.toUpperCase();
                }
                link += ('&specialcode=' + specialCode);
            }
            window.open(link, 'newwindow');
        } catch (err) {
            alert(err);
        } finally {
            return false;
        }
    }

    function prepareBookingForm() {
        try {
            jQuery(".ubicomp-form").each(function () {
                var form = this;

                if (is_multisite) {
                    var propertyCode = '<?php echo getSitePropertyCode(get_bloginfo()); ?>';
                    if (propertyCode) {
                        jQuery(form["property"]).val(propertyCode).change();
                    }
                }

                //cosmetics effect to auto select all night input text
                jQuery(form["night"]).on('focus', function () {
                    jQuery(this).select();
                });

                jQuery(form["adult"]).change(function () {
                    paxCountChange(form);
                });

                var $night = jQuery(form["night"]);
                var $checkin = jQuery(form["checkin"]);
                var $checkout = jQuery(form["checkout"]);
                $checkin.add($checkout).datepicker('destroy');

                var today = new Date();
                var default_min_checkindate = new Date(today);
                if (window.lead_hour && lead_hour > 0) {
                    default_min_checkindate = default_min_checkindate.setTime(default_min_checkindate.getTime() + (lead_hour * 3600 * 1000));
                }
                
                var default_min_checkoutdate = new Date(default_min_checkindate);
                default_min_checkoutdate = default_min_checkoutdate.setDate(default_min_checkoutdate.getDate() + 1);

                if (!wp_is_mobile) {
                    var opts = {
                        dateFormat: 'd M yy',
                        yearRange: '0:+5',
                        maxDate: '+5y',
                        altFormat: 'yy-mm-dd'
                    };

                    $checkin.datepicker(jQuery.extend(opts, {
                        altField: jQuery(form["alternate_checkin"]),
                        minDate: new Date(default_min_checkindate),
                        onSelect: function (dateText, inst) {
                            var dateForm = this.form;

                            var checkinDate = jQuery(this).datepicker("getDate");
                            var checkoutDate = jQuery(dateForm["checkout"]).datepicker("getDate");

                            var calcNumNight = (checkoutDate.getTime() - checkinDate.getTime()) / 1000 / 60 / 60 / 24;
                            if (calcNumNight < 1) {
                                //number of night must be at least 1 night
                                calcNumNight = 1;
                                jQuery(dateForm["night"]).val(calcNumNight).change();
                            } else {
                                jQuery(dateForm["night"]).val(calcNumNight);
                            }

                            var checkoutMinDate = new Date(checkinDate);
                            checkoutMinDate = checkoutMinDate.setDate(checkoutMinDate.getDate() + 1);

                            jQuery(dateForm["checkout"]).datepicker("option", "minDate", new Date(checkoutMinDate));
                        }
                    }));

                    $checkout.datepicker(jQuery.extend(opts, {
                        altField: jQuery(form["alternate_checkout"]),
                        minDate: new Date(default_min_checkoutdate),
                        onSelect: function (dateText, inst) {
                            //change checkout date should change only the number of nights
                            var dateForm = this.form;

                            var checkinDate = jQuery(dateForm["checkin"]).datepicker("getDate");
                            var checkoutDate = jQuery(this).datepicker("getDate");

                            var calcNumNight = (checkoutDate.getTime() - checkinDate.getTime()) / 1000 / 60 / 60 / 24;
                            jQuery(dateForm["night"]).val(calcNumNight);
                        }
                    }));

                    $checkin.datepicker("setDate", default_min_checkindate);
                } else {
                    //change both input type to date
                    $checkin.add($checkout).prop("type", "date").css({
                        'font-size': 'inherit'
                    });

                    var default_min_checkindate_ISO = formatISODate(default_min_checkindate);
                    $checkin.prop("min", default_min_checkindate_ISO);
                    $checkin.val(default_min_checkindate_ISO);

                    var default_min_checkoutdate_ISO = formatISODate(default_min_checkoutdate);
                    $checkout.prop("min", default_min_checkoutdate_ISO);

                    $checkin.on("change", function () {
                        var dateForm = this.form;

                        var checkinDate = parseISODate(jQuery(this).val());
                        var checkoutDate = parseISODate(jQuery(dateForm["checkout"]).val());

                        var calcNumNight = (checkoutDate.getTime() - checkinDate.getTime()) / 1000 / 60 / 60 / 24;
                        if (calcNumNight < 1) {
                            //number of night must be at least 1 night
                            calcNumNight = 1;
                            jQuery(dateForm["night"]).val(calcNumNight).change();
                        } else {
                            jQuery(dateForm["night"]).val(calcNumNight);
                        }

                        var checkoutMinDate = new Date(checkinDate);
                        checkoutMinDate = checkoutMinDate.setDate(checkoutMinDate.getDate() + 1);

                        jQuery(dateForm["checkout"]).prop("min", formatISODate(checkoutMinDate));
                    });

                    $checkout.on("change", function () {
                        var dateForm = this.form;

                        var checkinDate = parseISODate(jQuery(dateForm["checkin"]).val());
                        var checkoutDate = parseISODate(jQuery(this).val());

                        var calcNumNight = (checkoutDate.getTime() - checkinDate.getTime()) / 1000 / 60 / 60 / 24;
                        jQuery(dateForm["night"]).val(calcNumNight);
                    });

                    jQuery(window).resize(function () {
                        var windowWidth = jQuery('#calendar').width();
                        jQuery('#checkin').width(parseInt(windowWidth, 10) + 'px');
                        //jQuery('#checkout').width( parseInt((windowWidth * 0.75) - 10, 10) + 'px');
                    });

                }

                jQuery(form["night"]).change(function () {
                    var numNight = jQuery(this).val();
                    if (typeof numNight === "string") {
                        numNight = parseInt(numNight, 10);
                    }

                    var curentForm = this.form;
                    if (!wp_is_mobile) {
                        var checkinDate = jQuery(curentForm["checkin"]).datepicker("getDate");

                        var checkoutDate = new Date(checkinDate);
                        checkoutDate = checkoutDate.setDate(checkoutDate.getDate() + numNight);

                        jQuery(curentForm["checkout"]).datepicker("setDate", new Date(checkoutDate));
                    } else {
                        var checkinDate = parseISODate(jQuery(curentForm["checkin"]).val());

                        var checkoutDate = new Date(checkinDate);
                        checkoutDate = checkoutDate.setDate(checkoutDate.getDate() + numNight);

                        jQuery(curentForm["checkout"]).val(formatISODate(checkoutDate));
                    }
                }).change();
            });
        } catch (err) {
            console.error(err);
        }
    }

    var parseISODate = function (dateString) {
        return jQuery.datepicker.parseDate('yy-mm-dd', dateString);
    };

    var formatISODate = function (date) {
        if (typeof date !== "object") {
            date = new Date(date);
        }
        return jQuery.datepicker.formatDate('yy-mm-dd', date);
    };

    var paxCountChange = function (form) {
        var adult = jQuery(form["adult"]);
        var child = jQuery(form["child"]);
        var adultValue = parseInt(adult.val());
        var childValue = parseInt(child.val());
        maxOption(child, 3 - adultValue);
        if (adultValue === 3) {
            maxOption(child, 3 - adultValue);
        } else if (adultValue === 2) {
            maxOption(child, 1);
        } else if (adultValue === 1) {
            maxOption(child, 2);
        }
    };

    var maxOption = function (element, max) {
        if (!wp_is_mobile) {
            if (element.prop('name') === 'child') {
                element.val(0);
                jQuery(element).find('option').each(function () {
                    var _this = jQuery(this);
                    if (_this.val() > max) {
                        _this.hide();
                    } else {
                        _this.show();
                    }
                });
            }
        } else {
            // remove all option
            jQuery(element).find('option').remove().end();
            for (var i = 0; i <= max; i++) {
                jQuery(element).append(new Option(i, i));
            }
        }
    };

    var showMessage = function (message) {
        jQuery('#dialogDiv').text(message).dialog({
            autoOpen: true,
            buttons: [{
                    text: "OK",
                    icons: {
                        primary: "ui-icon-heart"
                    },
                    click: function () {
                        jQuery(this).text('').hide();
                        jQuery(this).dialog('close');
                    }
                }],
            closeOnEscape: true,
            dialogClass: "alert",
            draggable: false,
            modal: true,
            title: '',
            responsive: true,
            showTitleBar: false,
            showCloseButton: false
        }).focus();
        jQuery('.ui-widget-overlay').css('z-index', 1000);
        //	jQuery('.ui-dialog-titlebar').css('display','none');
        jQuery('.ui-dialog').css('z-index', 99999).focus();
    };
</script>