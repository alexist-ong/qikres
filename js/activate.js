(function ($) {
    $(document).ready(function () {
        $("#signup-content").addClass("content");
        $("#signup-content .wp-activate-container").addClass("container");

        $("#signup-content").css("min-height", function () {
            var foot_height = $("#foot").outerHeight(true);
            var content_margintop = $("#signup-content").css("margin-top");

            return "calc(100vh - " + foot_height + "px - " + content_margintop + ")";
        });
    });
})(jQuery);