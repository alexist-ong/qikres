<?php

/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 *
 */
function optionsframework_option_name() {

    // This gets the theme name from the stylesheet (lowercase and without spaces)
    $themename = get_option('stylesheet');
    $themename = preg_replace("/\W/", "_", strtolower($themename));

    $optionsframework_settings = get_option('optionsframework');
    $optionsframework_settings['id'] = $themename;
    update_option('optionsframework', $optionsframework_settings);

    // echo $themename;
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 */
function optionsframework_options() {
    // Pull all the categories into an array
    $options_categories = array();
    $options_categories_obj = get_categories();
    foreach ($options_categories_obj as $category) {
        $options_categories[$category->cat_ID] = $category->cat_name;
    }

    // Pull all tags into an array
    $options_tags = array();
    $options_tags_obj = get_tags();
    foreach ($options_tags_obj as $tag) {
        $options_tags[$tag->term_id] = $tag->name;
    }

    // Pull all the pages into an array
    $options_pages = array();
    $options_pages_obj = get_pages('sort_column=post_parent,menu_order');
    $options_pages[''] = 'Select a page:';
    foreach ($options_pages_obj as $page) {
        $options_pages[$page->ID] = $page->post_title;
    }

    // If using image radio buttons, define a directory path
    $imagepath = get_template_directory_uri() . '/images/';
    
    $options = array();
    
    /* GENERAL TAB */
    $options[] = array(
        'name' => __('General', 'options_check'),
        'type' => 'heading');
    
    $options[] = array(
        'name' => __('Content Top Margin', 'options_check'),
        'desc' => __('Content top margin', 'options_check'),
        'id' => 'content_margintop',
        'std' => '60px',
        'type' => 'text');
  
  	$options[] = array(
        'name' => __('Mobile Content Top Margin', 'options_check'),
        'desc' => __('Mobile content top margin. Empty to inherit from desktop content top margin', 'options_check'),
        'id' => 'mobile_content_margintop',
        'type' => 'text');
    
    $options[] = array(
        'name' => __('Font & Typography', 'options_check'),
        'type' => 'info');
    
    $options[] = array(
        'name' => __('Body Font', 'options_check'),
        'desc' => __('Body font settings.', 'options_check'),
        'id' => 'body_font',
        'std' => array(
            'size' => '12px',
            'face' => '',
            'style' => '',
            'color' => ''),
        'type' => 'typography');
    
    $options[] = array(
        'name' => __('Header H1 Font', 'options_check'),
        'desc' => __('Header h1 font settings.', 'options_check'),
        'id' => 'h1_font',
        'std' => array(
            'size' => '36px',
            'face' => '',
            'style' => '',
            'color' => ''),
        'type' => 'typography');
    
    $options[] = array(
        'name' => __('Header H2 Font', 'options_check'),
        'desc' => __('Header h2 font settings.', 'options_check'),
        'id' => 'h2_font',
        'std' => array(
            'size' => '30px',
            'face' => '',
            'style' => '',
            'color' => ''),
        'type' => 'typography');
    
    $options[] = array(
        'name' => __('Header H3 Font', 'options_check'),
        'desc' => __('Header h3 font settings.', 'options_check'),
        'id' => 'h3_font',
        'std' => array(
            'size' => '24px',
            'face' => '',
            'style' => '',
            'color' => ''),
        'type' => 'typography');
    
    $options[] = array(
        'name' => __('Header H4 Font', 'options_check'),
        'desc' => __('Header h4 font settings.', 'options_check'),
        'id' => 'h4_font',
        'std' => array(
            'size' => '18px',
            'face' => '',
            'style' => '',
            'color' => ''),
        'type' => 'typography');
    
    $options[] = array(
        'name' => __('Header H5 Font', 'options_check'),
        'desc' => __('Header h5 font settings.', 'options_check'),
        'id' => 'h5_font',
        'std' => array(
            'size' => '14px',
            'face' => '',
            'style' => '',
            'color' => ''),
        'type' => 'typography');
    
    $options[] = array(
        'name' => __('Header H6 Font', 'options_check'),
        'desc' => __('Header h6 font settings.', 'options_check'),
        'id' => 'h6_font',
        'std' => array(
            'size' => '12px',
            'face' => '',
            'style' => '',
            'color' => ''),
        'type' => 'typography');
    
    $options[] = array(
        'name' => __('Hyperlink Color', 'options_check'),
        'desc' => __('Hyperlink color.', 'options_check'),
        'id' => 'hyperlink_color',
        'type' => 'color');
    
    $options[] = array(
        'name' => __('Hyperlink Color on Hover', 'options_check'),
        'desc' => __('Hyperlink color on hover.', 'options_check'),
        'id' => 'hyperlink_hover_color',
        'type' => 'color');
    
    $options[] = array(
        'name' => __('Default Button Styling', 'options_check'),
        'type' => 'info');
    
    $options[] = array(
        'name' => __('Button Color', 'options_check'),
        'desc' => __('Button color.', 'options_check'),
        'id' => 'button_default_color',
        'type' => 'color');
    
    $options[] = array(
        'name' => __('Button Text Color', 'options_check'),
        'desc' => __('Button text color.', 'options_check'),
        'id' => 'button_default_text_color',
        'type' => 'color');
    
    $options[] = array(
        'name' => __('Button Color on Hover', 'options_check'),
        'desc' => __('Button color on hover.', 'options_check'),
        'id' => 'button_default_hover_color',
        'type' => 'color');
    
    $options[] = array(
        'name' => __('Button Text Color on Hover', 'options_check'),
        'desc' => __('Button text color on hover.', 'options_check'),
        'id' => 'button_default__hover_text_color',
        'type' => 'color');
    /* GENERAL TAB */

    /* HEADER TAB */
    $options[] = array(
        'name' => __('Header & Menu', 'options_check'),
        'type' => 'heading');
    
    $options[] = array(
        'name' => __('Primary Header & Menu Setting', 'options_check'),
        'type' => 'info');
    
    $options[] = array(
        'name' => __('Background Color', 'options_check'),
        'desc' => __('Header background color.', 'options_check'),
        'id' => 'header_bg_color',
        'type' => 'color');
    
    $options[] = array(
        'name' => __('Padding', 'options_check'),
        'desc' => __('Header padding.', 'options_check'),
  		'std' => '10px',
        'id' => 'header_padding',
        'type' => 'text');
	
	$options[] = array(
        'name' => __('Menu Font Size', 'options_check'),
        'desc' => __('Menu font size. Leave it empty to set to default.', 'options_check'),
        'id' => 'menu_font_size',
        'type' => 'text');
    
    $options[] = array(
        'name' => __('Menu Font Color', 'options_check'),
        'desc' => __('Menu text color', 'options_check'),
        'id' => 'menu_font_color',
        'type' => 'color');
  
  	$options[] = array(
        'name' => __('Selected Menu Font Color', 'options_check'),
        'desc' => __('Selected menu text color', 'options_check'),
        'id' => 'selected_menu_font_color',
        'type' => 'color');
    
    $options[] = array(
        'name' => __('Sub-menu Setting', 'options_check'),
        'type' => 'info');
    
    $options[] = array(
        'name' => __('Background Color', 'options_check'),
        'desc' => __('Sub-menu background color', 'options_check'),
        'id' => 'submenu_bg_color',
        'type' => 'color');
	
	$options[] = array(
        'name' => __('Menu Font Size', 'options_check'),
        'desc' => __('Sub-menu font size. Leave it empty to inherit from menu font size.', 'options_check'),
        'id' => 'submenu_font_size',
        'type' => 'text');
    
    $options[] = array(
        'name' => __('Menu Font Color', 'options_check'),
        'desc' => __('Sub-menu text color', 'options_check'),
        'id' => 'submenu_font_color',
        'type' => 'color');
  
  	$options[] = array(
        'name' => __('Selected Menu Font Color', 'options_check'),
        'desc' => __('Selected sub-menu text color', 'options_check'),
        'id' => 'selected_submenu_font_color',
        'type' => 'color'); 
    
    $options[] = array(
        'name' => __('Top Navigation Header & Menu Setting', 'options_check'),
        'type' => 'info');
    
    $options[] = array(
        'name' => __('Background Color', 'options_check'),
        'desc' => __('Top header background color.', 'options_check'),
        'id' => 'top_header_bg_color',
        'type' => 'color');
    
    $options[] = array(
        'name' => __('Padding', 'options_check'),
        'desc' => __('Top header padding.', 'options_check'),
  		'std' => '10px',
        'id' => 'top_header_padding',
        'type' => 'text');
    
    $options[] = array(
        'name' => __('Font', 'options_check'),
        'desc' => __('Top header font settings.', 'options_check'),
        'id' => 'top_menu_font',
        'std' => array(
            'size' => '13px',
            'face' => '',
            'style' => '',
            'color' => ''),
        'type' => 'typography');
  
  	$options[] = array(
        'name' => __('Mobile Menu Setting', 'options_check'),
        'type' => 'info');
    
    $options[] = array(
        'name' => __('Menu Highlight Color', 'options_check'),
        'desc' => __('Menu highlight color', 'options_check'),
        'id' => 'mobile_menu_highlight_color',
        'type' => 'color'); 
	
	$options[] = array(
        'name' => __('Guest Initial Token Setting', 'options_check'),
        'type' => 'info');
	
	$options[] = array(
        'name' => __('Background Color', 'options_check'),
        'desc' => __('Token background color', 'options_check'),
        'id' => 'guest_token_bg_color',
        'type' => 'color'); 
	
	$options[] = array(
        'name' => __('Font Color', 'options_check'),
        'desc' => __('Token font color', 'options_check'),
        'id' => 'guest_token_font_color',
        'type' => 'color'); 
    /* HEADER TAB */
    
    /* LOGO TAB */
    $options[] = array(
        'name' => __('Logo', 'options_check'),
        'type' => 'heading');
    
    $options[] = array(
        'name' => __('Logo Max Height', 'options_check'),
        'desc' => __('Logo maximum height.', 'options_check'),
        'id' => 'logo_max_height',
        'class' => 'mini',
        'type' => 'text');
  
  	$options[] = array(
        'name' => __('Logo Position', 'options_check'),
        'desc' => __('Position of logo.', 'options_check'),
        'id' => 'logo_position',
  		'std' => 'left',
		'type' => 'select',
        'class' => 'mini',
  		'options' => array(
            'left' => __('Left', 'options_check'),
            'top' => __('Top', 'options_check'),
            'right' => __('Right', 'options_check')
        ));
    /* LOGO TAB */
    
    /* FOOTER */
    $options[] = array(
        'name' => __('Footer', 'options_check'),
        'type' => 'heading');
    
    $options[] = array(
        'name' => __('Primary Footer Setting', 'options_check'),
        'type' => 'info');
    
    $options[] = array(
        'name' => __('Primary Background Color', 'options_check'),
        'desc' => __('Primary footer background color', 'options_check'),
        'id' => 'footer_primary_bg_color',
        'type' => 'color');
    
    $options[] = array(
        'name' => __('Primary Padding', 'options_check'),
        'desc' => __('Primary footer padding.', 'options_check'),
        'id' => 'footer_primary_padding',
        'type' => 'text');
    
    $options[] = array(
        'name' => __('Primary Header Font', 'options_check'),
        'desc' => __('Primary footer header font', 'options_check'),
        'id' => 'footer_primary_footer_font',
        'std' => array(
            'size' => '24px',
            'face' => '',
            'style' => '',
            'color' => ''),
        'type' => 'typography');
    
    $options[] = array(
        'name' => __('Primary Text Font', 'options_check'),
        'desc' => __('Primary footer text font', 'options_check'),
        'id' => 'footer_primary_font',
        'std' => array(
            'size' => '14px',
            'face' => '',
            'style' => '',
            'color' => ''),
        'type' => 'typography');
    
    $options[] = array(
        'name' => __('Secondary Footer Setting', 'options_check'),
        'type' => 'info');
    
    $options[] = array(
        'name' => __('Secondary Background Color', 'options_check'),
        'desc' => __('Secondary footer background color', 'options_check'),
        'id' => 'footer_secondary_bg_color',
        'type' => 'color');
    
    $options[] = array(
        'name' => __('Secondary Padding', 'options_check'),
        'desc' => __('Secondary footer padding.', 'options_check'),
        'id' => 'footer_secondary_padding',
        'type' => 'text');
    
    $options[] = array(
        'name' => __('Secondary Font', 'options_check'),
        'desc' => __('Secondary footer text', 'options_check'),
        'id' => 'footer_secondary_font',
        'std' => array(
            'size' => '14px',
            'face' => '',
            'style' => '',
            'color' => ''),
        'type' => 'typography');
    
    $options[] = array(
        'name' => __('Social Media Icon', 'options_check'),
        'type' => 'info');
    
    $options[] = array(
        'name' => __('Icon Size', 'options_check'),
        'desc' => __('Icon size. Empty to default the size to 25px.', 'options_check'),
        'id' => 'social_media_icon_size',
        'std' => '25px',
        'type' => 'text');
    
    $options[] = array(
        'name' => __('Icon Color', 'options_check'),
        'desc' => __('Icon color.', 'options_check'),
        'id' => 'social_media_icon_color',
        'type' => 'color');
    
    $options[] = array(
        'name' => __('Icon Color on Hover', 'options_check'),
        'desc' => __('Icon color on hover.', 'options_check'),
        'id' => 'social_media_icon_hover_color',
        'type' => 'color');
    /* FOOTER */
    
    /* Ubicompsystem Booking Form */
    $options[] = array(
        'name' => __('Booking Form', 'options_check'),
        'type' => 'heading');
    
    $options[] = array(
        'name' => __('Background Color', 'options_check'),
        'desc' => __('Background color', 'options_check'),
        'id' => 'ubicomp_bg_color',
        'std' => '#000000',
        'type' => 'color');
    
    $options[] = array(
        'name' => __('Background Opacity', 'options_check'),
        'desc' => __('Background opacity', 'options_check'),
        'id' => 'ubicomp_bg_opacity',
        'std' => '0.8',
        'type' => 'text');
    
    $options[] = array(
        'name' => __('Label Color', 'options_check'),
        'desc' => __('Label color', 'options_check'),
        'id' => 'ubicomp_label_color',
        'std' => '#ffffff',
        'type' => 'color');
    /* Ubicompsystem Booking Form */
    
    return $options;
}

function enqueue_additional_admin_css() {
	wp_enqueue_style( 'additonal_optionsframework_css', get_stylesheet_directory_uri() . '/css/optionsframework.css', array(),  Options_Framework::VERSION );
}

add_action( 'admin_enqueue_scripts', 'enqueue_additional_admin_css' );