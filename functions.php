<?php

include_once(ABSPATH . 'wp-admin/includes/plugin.php');
require_once('inc/class-tgm-plugin-activation.php');

try {
    defined('UBI_THEME') or define('UBI_THEME', '');

    if (!defined('UBI_HOST')) {
        add_action('admin_notices', 'ubi_host_missing_error');
    }

    if (!defined('UBI_CUSTOMER_ID')) {
        add_action('admin_notices', 'ubi_customer_missing_error');
    }

    if (!is_multisite() && !defined('UBI_PROPERTY_CODE')) {
        add_action('admin_notices', 'ubi_property_missing_error');
    }
    
    if (!extension_loaded('curl')){
        add_action('admin_notices', 'ubi_curl_not_activated');
    }
} catch (Exception $ex) {
    error_log($ex->getMessage());
}

function ubi_host_missing_error() {
    ubi_constant_missing_error('Constant UBI_HOST is missing. Qikres theme requires this constant to be specified. Please set it up on wp-config.php file.');
}

function ubi_customer_missing_error() {
    ubi_constant_missing_error('Constant UBI_CUSTOMER_ID is missing. Qikres theme requires this constant to be specified. Please set it up on wp-config.php file.');
}

function ubi_property_missing_error() {
    ubi_constant_missing_error('Constant UBI_PROPERTY_CODE is missing. Qikres theme requires this constant to be specified. Please set it up on wp-config.php file.');
}

function ubi_curl_not_activated() {
    ubi_constant_missing_error('Extension cURL not enabled/installed. Qikres theme requires this extension to be installed and enabled in order to work property.');
}

function ubi_constant_missing_error($message) {
    ?>
    <div class="notice notice-warning is-dismissible">
        <p><?php echo $message; ?></p>
    </div>
    <?php
}

/**
 * Enqueue parent theme style.css
 */
function enqueue_parent_styles() {
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
}
add_action('wp_enqueue_scripts', 'enqueue_parent_styles');

function qikres_i18n_setup() {
    load_theme_textdomain('qikres', get_stylesheet_directory() . '/languages');
}
add_action('after_setup_theme', 'qikres_i18n_setup');

function qikres_homepage() {    
    if (is_multisite()) {
        // still have no idea how to translate mulisite :(
        return network_site_url();
    }
    
    $home_page = home_url();
    if (function_exists('pll_home_url')) {
        $home_page = pll_home_url();
    }
    
    return $home_page;
}

function qikres_register_required_plugins() {
    $plugins = array(
		array(
			'name' => 'Options Framework',
			'slug' => 'options-framework',
			'source' => 'https://gitlab.com/api/v4/projects/ubicompsystem%2Foptions-framework-plugin/repository/archive.zip',
            'required' => true,
            'force_activation' => true,
			'force_deactivation' => true
		),
        array(
			'name' => 'WP Session Manager',
			'slug' => 'wp-session-manager',
            'required' => true,
            'force_activation' => true,
			'force_deactivation' => true
		),
        array(
            'name' => 'Github Updater',
            'slug' => 'github-updater',
            'source' => 'https://github.com/afragen/github-updater/archive/master.zip',
            'required' => false
        ),
        array(
			'name' => 'Polylang',
			'slug' => 'polylang',
			'required' => false
		)
	);
    
    $config = array(
		'id' => 'tgmpa',
        'default_path' => '',
        'menu' => 'tgmpa-install-plugins',
        'parent_slug' => 'themes.php',
        'capability' => 'edit_theme_options',
        'has_notices' => true,
        'dismissable' => true,
        'dismiss_msg' => '',
        'is_automatic' => false,
        'message' => ''
	);

	tgmpa( $plugins, $config );
}
add_action('tgmpa_register', 'qikres_register_required_plugins');

function strStartsWith($haystack, $needle) {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function strEndsWith($haystack, $needle) {
    $length = strlen($needle);

    return $length === 0 || (substr($haystack, -$length) === $needle);
}

function change_sow_button_default_styles($form_options, $widget) {
    if (is_plugin_active('options-framework/options-framework.php') &&
            (get_class($widget) == 'SiteOrigin_Widget_Button_Widget' || get_class($widget) == 'Book_Now_Button_Widget')
    ) {
        $default_button_color = of_get_option('button_default_color');
        $default_button_text_color = of_get_option('button_default_text_color');

        if ($default_button_color) {
            $form_options['design']['fields']['button_color']['default'] = $default_button_color;
        }

        if ($default_button_text_color) {
            $form_options['design']['fields']['text_color']['default'] = $default_button_text_color;
        }
    }
    return $form_options;
}
add_filter('siteorigin_widgets_form_options_sow-button', 'change_sow_button_default_styles', 10, 2);
add_filter('siteorigin_widgets_form_options_book-now-button-widget', 'change_sow_button_default_styles', 10, 2);

function get_google_fonts_conf($font) {
    // Google Font Defaults
    $google_fonts_confs = array(
        'Raleway' => '100,300,300i,400,400i,600,600i,700,700i&subset=latin,latin,latin,latin,latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese,latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese,latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese',
        'Open Sans' => '300,300i,400,400i,600,600i,700,700i&subset=latin,latin,latin,latin,latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese,latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese,latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese',
        'Playfair Display' => '400,400i,600,600i,700,700i,900,900i&subset=latin,latin,latin,latin,latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese,latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese,latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese'
    );

    $google_fonts_confs = apply_filters('qikres_google_fonts', $google_fonts_confs);

    $conf = '';
    if (array_key_exists($font, $google_fonts_confs)) {
        $conf = $google_fonts_confs[$font];
    }

    return $conf;
}

function options_typography_get_google_fonts() {
    // Google Font Defaults
    $google_faces = array(
        'Droid Sans, sans-serif' => 'Droid Sans',
        'Droid Serif, serif' => 'Droid Serif',
        'Lato, sans-serif' => 'Lato',
        'Montserrat, sans-serif' => 'Montserrat',
        'Open Sans, sans-serif' => 'Open Sans',
        'Playfair Display, serif' => 'Playfair Display',
        'PT Sans, sans-serif' => 'PT Sans',
        'Raleway, cursive' => 'Raleway',
        'Roboto, sans-serif' => 'Roboto',
        'Tinos, serif' => 'Tinos',
        'Yantramanav, sans-serif' => 'Yantramanav'
    );

    $google_faces = apply_filters('qikres_google_faces', $google_faces);

    return $google_faces;
}

function add_google_fonts_list($defaults) {
    return array_merge(array('' => ''), $defaults, options_typography_get_google_fonts());
}
add_filter('of_recognized_font_faces', 'add_google_fonts_list');

function add_empty_font_styles($defaults) {
    return array_merge(array('' => ''), $defaults);
}
add_filter('of_recognized_font_styles', 'add_empty_font_styles');

/**
 * Checks font options to see if a Google font is selected.
 * If so, options_typography_enqueue_google_font is called to enqueue the font.
 * Ensures that each Google font is only enqueued once.
 */
if (!function_exists('options_typography_google_fonts') && function_exists('of_get_option')) {
    function options_typography_google_fonts() {
        $all_google_fonts = array_keys(options_typography_get_google_fonts());

        // Get the font face for each option and put it in an array
        $selected_fonts = array();
        $font_options = array('body_font', 'h1_font', 'h2_font', 'h3_font', 'h4_font', 'h5_font', 'h6_font', 'footer_primary_font', 'footer_primary_footer_font', 'footer_secondary_font');
        foreach ($font_options as $font_opt) {
            if (of_get_option($font_opt)) {
                $font = of_get_option($font_opt);
                array_push($selected_fonts, $font['face']);
            }
        }

        // Remove any duplicates in the list
        $selected_fonts = array_unique($selected_fonts);
        // Check each of the unique fonts against the defined Google fonts
        // If it is a Google font, go ahead and call the function to enqueue it
        foreach ($selected_fonts as $font) {
            if (in_array($font, $all_google_fonts)) {
                options_typography_enqueue_google_font($font);
            }
        }
    }    
}

if (function_exists('options_typography_google_fonts')) {
    add_action('wp_enqueue_scripts', 'options_typography_google_fonts');
}

/**
 * Enqueues the Google $font that is passed
 */
function options_typography_enqueue_google_font($font) {
    $font = explode(',', $font);
    $font = $font[0];

    $conf = get_google_fonts_conf($font);
    if (!empty($conf)) {
        $font .= ':' . $conf;
    }

    $font = str_replace(' ', '+', $font);
    wp_enqueue_style("options_typography_$font", "https://fonts.googleapis.com/css?family=$font", false, null, 'all');
}

function custom_filter_body_class($classes) {
    if (wp_is_mobile()) {
        $classes[] = 'is-mobile';
    }
    return $classes;
}
add_filter('body_class', 'custom_filter_body_class');

function getSiteProperties() {
    $site_properties = array();
    $site_properties = apply_filters('qikres_site_property', $site_properties);

    return $site_properties;
}

function getSitePropertyCode($site_name) {
    $propertyCode = '';

    //decode escaped character such as &
    $site_name = html_entity_decode($site_name);

    $site_properties = getSiteProperties();
    foreach ($site_properties as $key => $value) {
        if (strtolower($site_name) == strtolower($value)) {
            $propertyCode = $key;
            break;
        }
    }

    return $propertyCode;
}

/**
 * Allow underscored usernames
 *
 * @wp-hook wpmu_validate_user_signup
 * @param   array $result
 * @return  array
 */
function allow_underscored_usernames($result) {
    $error_name = $result['errors']->get_error_message('user_name');
    if (!empty($error_name) && $error_name == __('Usernames can only contain lowercase letters (a-z) and numbers.') && $result['user_name'] == $result['orig_username'] && !preg_match('/[^_a-z0-9]/', $result['user_name'])
    ) {
        unset($result['errors']->errors['user_name']);
        return $result;
    } else {
        return $result;
    }
}
add_filter('wpmu_validate_user_signup', 'allow_underscored_usernames');

/**
 * Get around the problem with wpmu_create_blog() where sanitize_user()  
 * strips out the semicolon (:) in the $domain string
 * This means created sites with hostnames of 
 * e.g. example.tld88 instead of example.tld:88
 */
function allow_site_with_port($username, $raw_username, $strict) {
    // Edit the port to your needs
    $port = 88;

    if ($strict                                                // wpmu_create_blog uses strict mode
            && is_multisite()                                         // multisite check
            && $port == parse_url($raw_username, PHP_URL_PORT)      // raw domain has port 
            && false === strpos($username, ':' . $port)             // stripped domain is without correct port
    ) {
        $username = str_replace($port, ':' . $port, $username); // replace e.g. example.tld88 to example.tld:88
    }
    return $username;
}
add_filter('sanitize_user', 'allow_site_with_port', 1, 3);

function create_property_sites($atts = [], $content = null) {
    $output = '<select';

    foreach ($atts as $key => $value) {
        $output .= ' ' . $key . '="' . esc_html($value) . '"';
    }

    $output .= '>';

    $site_properties = getSiteProperties();
    foreach ($site_properties as $key => $value) {
        $output .= '<option value="' . esc_html($key) . '">' . esc_html($value) . '</option>';
    }
    $output .= '</select>';

    return $output;
}
add_shortcode('site_property_list', 'create_property_sites');

/**
 * Register custom menu
 */
function register_custom_menu() {
    register_nav_menus(
            array(
                'top-nav-menu' => __('Top navigation menu')
            )
    );
}
add_action('init', 'register_custom_menu');


/**
 * Register translation string with polylang
 */
if (function_exists('pll_register_string')) {
    function qikres_pll_register_string() {
        pll_register_string('dess_copyright', get_theme_mod('dess_copyright'), 'Qikres Theme', true);
    }

    add_action('after_setup_theme', 'qikres_pll_register_string');
}

/**
 * Get current language code from polylang
 */
function getLanguageCode() {
    $language_code = '';
    if (function_exists('pll_current_language')) {
        $language_code = pll_current_language();
    }
    return $language_code;
}

/**
 * Get current post meta data
 */
function getMetaValue($key) {
    $values = get_post_custom_values($key);

    $value = '';
    if (!empty($values)) {
        $value = $values[0];
    }
    return $value;
}

/**
 * Remove action/filter from parent theme because it is not pluggable
 */
function remove_parent_theme_hook() {
    remove_action('customize_register', 'dess_customize_register');
    remove_filter('wp_title', 'dess_page_title');
}
add_action('after_setup_theme', 'remove_parent_theme_hook');

/**
 * Re-write the removed parent action function
 */
function child_customize_register($wp_customize) {
    $wp_customize->add_section(
            'header_section', array(
                'title' => __('Logo', 'multimedia'),
                'capability' => 'edit_theme_options',
                'description' => __('Allows you to edit your theme\'s layout.', 'multimedia')
            )
    );
    $wp_customize->add_setting('dess_logo', array(
        'default' => get_stylesheet_directory_uri() . '/images/logo.png',
        'type' => 'theme_mod',
        'sanitize_callback' => 'sanitize_customizer_val'
    ));
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'logo', array(
        'label' => __('Logo Image', 'multimedia'),
        'section' => 'header_section',
        'settings' => 'dess_logo'
    )));
    $wp_customize->add_section(
            'sm_section', array(
                'title' => __('Social Media', 'multimedia'),
                'capability' => 'edit_theme_options',
                'description' => __('Allows you to set your social media URLs', 'multimedia')
            )
    );
    $socials = array('twitter', 'facebook', 'google-plus', 'instagram', 'pinterest', 'linkedin', 'vimeo', 'youtube');
    for ($i = 0; $i < count($socials); $i++) {
        $name = str_replace('-', ' ', ucfirst($socials[$i]));
        $wp_customize->add_setting('dess_' . $socials[$i], array(
            'capability' => 'edit_theme_options',
            'type' => 'theme_mod',
            'sanitize_callback' => 'sanitize_customizer_val'
        ));
        $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'dess_' . $socials[$i], array(
            'settings' => 'dess_' . $socials[$i],
            'label' => $name . ' URL',
            'section' => 'sm_section',
            'type' => 'text'
        )));
    }
    $wp_customize->add_section(
            'featured_text_section', array(
                'title' => __('Featured Text', 'multimedia'),
                'capability' => 'edit_theme_options',
                'description' => __('Allows you to set your footer settings', 'multimedia')
            )
    );
    $wp_customize->add_setting('dess_hometext', array(
        'capability' => 'edit_theme_options',
        'type' => 'theme_mod',
        'sanitize_callback' => 'sanitize_customizer_val'
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'dess_hometext', array(
        'settings' => 'dess_hometext',
        'label' => __('Featured Text', 'multimedia'),
        'section' => 'featured_text_section',
        'type' => 'textarea'
    )));
    $wp_customize->add_section(
            'copyright_section', array(
                'title' => __('Copyright Text', 'multimedia'),
                'capability' => 'edit_theme_options',
                'description' => __('Allows you to set your footer settings', 'multimedia')
            )
    );
    $wp_customize->add_setting('dess_copyright', array(
        'capability' => 'edit_theme_options',
        'type' => 'theme_mod'
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'dess_copyright', array(
        'settings' => 'dess_copyright',
        'label' => __('Copyright Text', 'multimedia'),
        'section' => 'copyright_section',
        'type' => 'textarea'
    )));
}
add_action('customize_register', 'child_customize_register', 20);

/**
 * Load theme specific plugin on wp-activate.php page
 */
function load_site_specific_plugin_on_wp_active() {
    if (wp_installing()) {
        if (is_plugin_active('options-framework/options-framework.php')) {
            require_once (WP_PLUGIN_DIR . '/options-framework/options-framework.php');
        }
    }

    return;
}
add_action('activate_header', 'load_site_specific_plugin_on_wp_active');

/*
* Add scripts to wp-activate.php page
*/
function wps_add_activate_script() {
    add_action('wp_enqueue_scripts', 'enqueue_activate_scripts');
}
add_action( 'activate_header' , 'wps_add_activate_script' );

function enqueue_activate_scripts() {
    wp_enqueue_script( 'enqueue_activate_scripts', get_stylesheet_directory_uri() . '/js/activate.js', array( 'jquery') );
}

/**
 * Enqueue asset *.js and *.css
 */
function enqueue_qikres_assets() {
    global $wp_scripts;
    
    wp_enqueue_script( 'jquery-ui-core' );
    wp_enqueue_script( 'jquery-ui-datepicker' );
    
    // get registered script object for jquery-ui
    $ui = $wp_scripts->query( 'jquery-ui-core' );
    
    // default to smoothness, but can be changed with filter 'qikres_jquery_ui_theme'
    $ui_theme = 'smoothness';
    $ui_theme = apply_filters('qikres_jquery_ui_theme', $ui_theme);
    
    $ui_version = $ui->ver;
 
    // tell WordPress to load the jQuery UI theme from Google CDN
    $protocol = is_ssl() ? 'https' : 'http';
    $jquery_ui_url = "$protocol://ajax.googleapis.com/ajax/libs/jqueryui/{$ui_version}/themes/$ui_theme/jquery-ui.min.css";  
    
    // or developer could choose to change the jQuery UI source
    $jquery_ui_url = apply_filters('qikres_jquery_ui_url', $jquery_ui_url, $protocol, $ui_theme, $ui_version);    
    wp_enqueue_style("jquery-ui-$ui_theme", $jquery_ui_url);
    
    // enqueue local Font Awesome 4.7 in order to make full use of swap font display
    wp_enqueue_style('qikres-font-awesome', get_stylesheet_directory_uri() . '/lib/font-awesome/css/font-awesome.min.css');
}
add_action('wp_enqueue_scripts', 'enqueue_qikres_assets');

/**
 * Filter to display Qikres theme title
 */
function qikres_title($title = '', $sep = '|', $seplocation = 'right') {
    //Fix home page title
    if( empty( $title ) && ( is_home() || is_front_page() ) ) {
        $title = __( 'Home', 'multimedia' );
    }
    
    $tagline = '';
    $padded_sep = ' ' . $sep . ' ';
    if (!empty(get_bloginfo())) {
        $tagline .=  get_bloginfo();

        if (!empty(get_bloginfo('description'))) {
            $tagline .= ' - ';
        }
    }

    if (!empty(get_bloginfo('description'))) {
        $tagline .= get_bloginfo('description');
    }
    
    if (empty($tagline)) {
        $trimmed_title = rtrim($title, $padded_sep);
        $trimmed_title = ltrim($trimmed_title, $padded_sep);
        
        return $trimmed_title;
    }
    
    if ($seplocation == 'left') {
        return $tagline . (strStartsWith($title, $padded_sep)? '' : $padded_sep) . $title;
    } 
    return $title . (strEndsWith($title, $padded_sep)? '' : $padded_sep) . $tagline;
}
add_filter( 'wp_title', 'qikres_title', 10, 3 );

/**
 * Get property settings
 */
function get_property_settings() {
    $property_settings = isset($_SESSION['property_settings'])? $_SESSION['property_settings'] : new stdClass();
    if (empty((array)$property_settings)) {
        //if no cached property settings, we have to retrieve it  
        $property_code = is_multisite() ? getSitePropertyCode(get_bloginfo()) : (defined('UBI_PROPERTY_CODE')? UBI_PROPERTY_CODE : '');
        $qikres_host = defined('UBI_HOST')? UBI_HOST : '';

        if (!empty($property_code) && !empty($qikres_host)) {
            try {
                $url = add_query_arg(
                        array(
                            'propertyCode' => $property_code
                        ),
                        "$qikres_host/qikstay/property/detail.action");

                $response = wp_remote_get($url, array(
                    'timeout' => 10
                ));
                if (is_wp_error($response)) {
                    throw new Exception($response->get_error_message());
                }

                $body = wp_remote_retrieve_body($response);
                $property_settings = json_decode($body);
                
                $_SESSION['property_settings'] = $property_settings;
            } catch (Exception $ex) {
                error_log($ex->getMessage());
            }
        }
    }
    
    return $property_settings;
}

/**
 * Append chinese font family alternative
 */
function prepend_options_chinese_font($font_face = '') {
    if (!empty($font_face)) {
        $chinese_fonts = 'SimSun, Microsoft YaHei';
        
        if (strpos($font_face, 'sans-serif') !== false) {
            $font_face = str_ireplace('sans-serif', "$chinese_fonts, sans-serif", $font_face);
        } else if (strpos($font_face, 'serif') !== false) {
            $font_face = str_ireplace('serif', "$chinese_fonts, serif", $font_face);
        } else {
            $font_face .= $chinese_fonts;
        }
    }
    return $font_face;
}
add_filter('modify_options_font_face', 'prepend_options_chinese_font');

/**
 * Dequeue Lato font face from parent theme
 */
function dequeue_google_lato() {
   wp_dequeue_style( 'google-lato-font' );
}
add_action('wp_print_styles', 'dequeue_google_lato', 100);