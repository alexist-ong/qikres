<?php
get_header();
?>
<div class="content">
    <div class="container">
        <div class="post_content">
            <article class="post_content">
                <div class="error404--content">
                    <div><h1 class="error404--status-code">404</h1></div>
                    <div>
                        <h1 class="error404--status-desc">PAGE NOT FOUND</h1>
                        <p>The page you are looking for might have been removed, had its name changed or is temporarily unavailable.</p>
                        <p class="error404--return-link"><a class="button" href="<?php echo get_site_url(); ?>">&larr; Return to home page</a></p>
                    </div>
                </div>				
            </article>
        </div>
    </div>
</div>
<?php
get_footer();
?>