<?php 

function hexToRgb($hex, $alpha = false) {
    $hex = str_replace('#', '', $hex);
    $length = strlen($hex);
    $rgb['r'] = hexdec($length == 6 ? substr($hex, 0, 2) : ($length == 3 ? str_repeat(substr($hex, 0, 1), 2) : 0));
    $rgb['g'] = hexdec($length == 6 ? substr($hex, 2, 2) : ($length == 3 ? str_repeat(substr($hex, 1, 1), 2) : 0));
    $rgb['b'] = hexdec($length == 6 ? substr($hex, 4, 2) : ($length == 3 ? str_repeat(substr($hex, 2, 1), 2) : 0));
    if ($alpha) {
        $rgb['a'] = $alpha;
    }
    return implode(array_keys($rgb)) . '(' . implode(', ', $rgb) . ')';
} 

function getPaddingComponents($padding) {
    $padding_components = preg_split( '/\s+/', $padding, -1, PREG_SPLIT_NO_EMPTY );
    
    $padding_top = $padding_components[0];
    $padding_bottom = $padding_top;
    if( sizeof($padding_components) > 2 ) {
        $padding_bottom = $padding_components[2];
    } 
    
    $padding_right = $padding_top;
    if( sizeof($padding_components) > 1 ) {
        $padding_right = $padding_components[1];
    }
    
    $padding_left = $padding_right;
    if( sizeof($padding_components) > 3 ) {
        $padding_left = $padding_components[3];
    }
    
    return array(
        'top' => $padding_top,
        'right' => $padding_right,
        'bottom' => $padding_bottom,
        'left' => $padding_left
    );
}

?>
<style type="text/css">
    body { <?php
    if( of_get_option( 'body_font' ) ) { 
        $font = of_get_option( 'body_font' );
        $font_face = apply_filters('modify_options_font_face', $font['face']);
        $font_size = $font['size'];
        $font_color = $font['color'];
        $font_style = $font['style'];
        
        if($font_face) { ?>
        font-family: <?php echo $font_face; ?>; <?php            
        }
        
        if($font_size) { ?>
        font-size: <?php echo $font_size; ?>; <?php            
        }
        
        if($font_color) { ?>
        color: <?php echo $font_color; ?>; <?php            
        }
        
        if($font_style) {
            $font_style_component = explode(" ", $font_style);
            
            foreach($font_style_component as $value) {
                if($value == 'italic') { ?>
        font-style: <?php echo $value; ?>; <?php
                } elseif($value == 'normal' || $value == 'bold') { ?>
        font-weight: <?php echo $value; ?>; <?php                    
                }                
            }
        }
    } ?>
    }
    
    h1,
	.blog_post_box h1,
	.post_content .post_box h1 { <?php
    if( of_get_option( 'h1_font' ) ) { 
        $font = of_get_option( 'h1_font' );
        $font_face = apply_filters('modify_options_font_face', $font['face']);
        $font_size = $font['size'];
        $font_color = $font['color'];
        $font_style = $font['style'];
        
        if($font_face) { ?>
        font-family: <?php echo $font_face; ?>; <?php            
        }
        
        if($font_size) { ?>
        font-size: <?php echo $font_size; ?>; <?php            
        }
        
        if($font_color) { ?>
        color: <?php echo $font_color; ?>; <?php            
        }
        
        if($font_style) {
            $font_style_component = explode(" ", $font_style);
            
            foreach($font_style_component as $value) {
                if($value == 'italic') { ?>
        font-style: <?php echo $value; ?>; <?php
                } elseif($value == 'normal' || $value == 'bold') { ?>
        font-weight: <?php echo $value; ?>; <?php                    
                }                
            }
        }
    } ?>
    }
    
    h2,
	.blog_post_box h2,
	.post_content .post_box h2 { <?php
    if( of_get_option( 'h2_font' ) ) { 
        $font = of_get_option( 'h2_font' );
        $font_face = apply_filters('modify_options_font_face', $font['face']);
        $font_size = $font['size'];
        $font_color = $font['color'];
        $font_style = $font['style'];
        
        if($font_face) { ?>
        font-family: <?php echo $font_face; ?>; <?php            
        }
        
        if($font_size) { ?>
        font-size: <?php echo $font_size; ?>; <?php            
        }
        
        if($font_color) { ?>
        color: <?php echo $font_color; ?>; <?php            
        }
        
        if($font_style) {
            $font_style_component = explode(" ", $font_style);
            
            foreach($font_style_component as $value) {
                if($value == 'italic') { ?>
        font-style: <?php echo $value; ?>; <?php
                } elseif($value == 'normal' || $value == 'bold') { ?>
        font-weight: <?php echo $value; ?>; <?php                    
                }                
            }
        }
    } ?>
    }
    
    h3, 
	.blog_post_box h3, 
	.post_content .post_box h3 { <?php
    if( of_get_option( 'h3_font' ) ) { 
        $font = of_get_option( 'h3_font' );
        $font_face = apply_filters('modify_options_font_face', $font['face']);
        $font_size = $font['size'];
        $font_color = $font['color'];
        $font_style = $font['style'];
        
        if($font_face) { ?>
        font-family: <?php echo $font_face; ?>; <?php            
        }
        
        if($font_size) { ?>
        font-size: <?php echo $font_size; ?>; <?php            
        }
        
        if($font_color) { ?>
        color: <?php echo $font_color; ?>; <?php            
        }
        
        if($font_style) {
            $font_style_component = explode(" ", $font_style);
            
            foreach($font_style_component as $value) {
                if($value == 'italic') { ?>
        font-style: <?php echo $value; ?>; <?php
                } elseif($value == 'normal' || $value == 'bold') { ?>
        font-weight: <?php echo $value; ?>; <?php                    
                }                
            }
        }
    } ?>
    }
    
    h4,
	.blog_post_box h4,
	.post_content .post_box h4 { <?php
    if( of_get_option( 'h4_font' ) ) { 
        $font = of_get_option( 'h4_font' );
        $font_face = apply_filters('modify_options_font_face', $font['face']);
        $font_size = $font['size'];
        $font_color = $font['color'];
        $font_style = $font['style'];
        
        if($font_face) { ?>
        font-family: <?php echo $font_face; ?>; <?php            
        }
        
        if($font_size) { ?>
        font-size: <?php echo $font_size; ?>; <?php            
        }
        
        if($font_color) { ?>
        color: <?php echo $font_color; ?>; <?php            
        }
        
        if($font_style) {
            $font_style_component = explode(" ", $font_style);
            
            foreach($font_style_component as $value) {
                if($value == 'italic') { ?>
        font-style: <?php echo $value; ?>; <?php
                } elseif($value == 'normal' || $value == 'bold') { ?>
        font-weight: <?php echo $value; ?>; <?php                    
                }                
            }
        }
    } ?>
    }
    
    h5,
	.blog_post_box h5,
	.post_content .post_box h5 { <?php
    if( of_get_option( 'h5_font' ) ) { 
        $font = of_get_option( 'h5_font' );
        $font_face = apply_filters('modify_options_font_face', $font['face']);
        $font_size = $font['size'];
        $font_color = $font['color'];
        $font_style = $font['style'];
        
        if($font_face) { ?>
        font-family: <?php echo $font_face; ?>; <?php            
        }
        
        if($font_size) { ?>
        font-size: <?php echo $font_size; ?>; <?php            
        }
        
        if($font_color) { ?>
        color: <?php echo $font_color; ?>; <?php            
        }
        
        if($font_style) {
            $font_style_component = explode(" ", $font_style);
            
            foreach($font_style_component as $value) {
                if($value == 'italic') { ?>
        font-style: <?php echo $value; ?>; <?php
                } elseif($value == 'normal' || $value == 'bold') { ?>
        font-weight: <?php echo $value; ?>; <?php                    
                }                
            }
        }
    } ?>
    }
    
    h6,
	.blog_post_box h6,
	.post_content .post_box h6 { <?php
    if( of_get_option( 'h6_font' ) ) { 
        $font = of_get_option( 'h6_font' );
        $font_face = apply_filters('modify_options_font_face', $font['face']);
        $font_size = $font['size'];
        $font_color = $font['color'];
        $font_style = $font['style'];
        
        if($font_face) { ?>
        font-family: <?php echo $font_face; ?>; <?php            
        }
        
        if($font_size) { ?>
        font-size: <?php echo $font_size; ?>; <?php            
        }
        
        if($font_color) { ?>
        color: <?php echo $font_color; ?>; <?php            
        }
        
        if($font_style) {
            $font_style_component = explode(" ", $font_style);
            
            foreach($font_style_component as $value) {
                if($value == 'italic') { ?>
        font-style: <?php echo $value; ?>; <?php
                } elseif($value == 'normal' || $value == 'bold') { ?>
        font-weight: <?php echo $value; ?>; <?php                    
                }                
            }
        }
    } ?>
    }
    
    a, .post_content .post_box a { <?php
    if( of_get_option( 'hyperlink_color' ) ) { ?>
        color: <?php echo of_get_option('hyperlink_color'); ?>; <?php        
    } ?>
    }
    
    a:hover, .post_content .post_box a:hover { <?php
    if( of_get_option( 'hyperlink_hover_color' ) ) { ?>
        color: <?php echo of_get_option('hyperlink_hover_color'); ?>; <?php        
    } ?>
    }
    
    input[type="submit"], 
    input[type="button"], 
    button { <?php
    if( of_get_option( 'button_default_color' ) ) { ?>
        background-color: <?php echo of_get_option('button_default_color'); ?>;
        border-color: <?php echo of_get_option('button_default_color'); ?>; <?php        
    }
    
    if( of_get_option( 'button_default_text_color' ) ) { ?>
        color: <?php echo of_get_option('button_default_text_color'); ?>; <?php        
    } ?>
    }
    
    input[type="submit"]:hover, 
    input[type="button"]:hover, 
    button:hover { <?php
    if( of_get_option( 'button_default_hover_color' ) ) { ?>
        background-color: <?php echo of_get_option('button_default_hover_color'); ?>;
        border-color: <?php echo of_get_option('button_default_hover_color'); ?>; <?php        
    }
    
    if( of_get_option( 'button_default_hover_text_color' ) ) { ?>
        color: <?php echo of_get_option('button_default_hover_text_color'); ?>; <?php        
    } ?>
    }
    
    .head-area { <?php    
    if( of_get_option( 'header_padding' ) ) { ?>
        padding: 0; <?php    
    } ?>
    }

	.head-menu-nav { <?php 
    if( of_get_option( 'logo_position' ) ) {
        $logo_position = of_get_option( 'logo_position' );

        if( $logo_position == 'right' ) { ?>
          	-webkit-box-orient: horizontal;
          	-webkit-box-direction: reverse;
          	-ms-flex-direction: row-reverse;
          	flex-direction: row-reverse; <?php          	
        } elseif ( $logo_position == 'top' ) { ?>
          	display: block; <?php          	
        }  
    }
    
    if( of_get_option( 'header_bg_color' ) ) { ?>
        background-color: <?php echo of_get_option('header_bg_color'); ?>; <?php        
    }
    
    if( of_get_option( 'header_padding' ) ) { ?>
        padding: <?php echo of_get_option('header_padding'); ?>; <?php    
    } ?>
    }

    .head-menu-nav .logo { <?php 
    if( of_get_option( 'logo_position' ) ) {
        $logo_position = of_get_option( 'logo_position' );

        if( $logo_position == 'top' ) { ?>
          	float: none;
          	text-align: center; <?php          	
        }
    } ?>
    }

    .top-nav + .head-menu-nav .logo { <?php
    if( of_get_option( 'logo_position' ) ) {
        $logo_position = of_get_option( 'logo_position' );

        if( $logo_position == 'left' || $logo_position == 'right' ) {
            $topnav_padding = ( of_get_option( 'top_header_padding' )? : '10px' );
            $topnav_padding_components = getPaddingComponents( $topnav_padding );
            
            $topnav_padding_top = $topnav_padding_components['top'];
            $topnav_padding_bottom = $topnav_padding_components['bottom']; ?>
                margin-top: calc(-20px - <?php echo $topnav_padding_top; ?> - <?php echo $topnav_padding_bottom; ?>); <?php
        }
    } 
    ?>
    }
    
	.head-menu-nav .head-nav { <?php 
  	if( of_get_option( 'logo_position' ) ) {
    	$logo_position = of_get_option( 'logo_position' );
    
    	if( $logo_position == 'top' ) { ?>
      		float: none;
      		text-align: center;
      		margin-top: 20px; <?php      
    	} 
  	} ?>
	}
    
    .top-nav { <?php 
  	if( of_get_option( 'top_header_bg_color' ) ) { ?>
        background-color: <?php echo of_get_option('top_header_bg_color'); ?>; <?php        
    } 
    
    if( of_get_option( 'top_header_padding' ) ) { ?>
        padding: <?php echo of_get_option('top_header_padding'); ?>; <?php    
    } ?>
    }
	
	.top-nav + .head-menu-nav .head-nav { <?php 
  	if( of_get_option( 'logo_position' ) ) {
    	$logo_position = of_get_option( 'logo_position' );
    
    	if( $logo_position == 'left' || $logo_position == 'right' ) { ?>
			-webkit-align-self: flex-end;
    		-ms-flex-item-align: end;
        		align-self: flex-end; <?php			
		}
  	} ?>		
	}
    
    .logo a { <?php
    if( of_get_option( 'logo_max_height' ) && of_get_option( 'logo_max_height' ) != 'none' ) { ?>
        display: inline-block;
        max-height: <?php echo of_get_option('logo_max_height'); ?>; <?php        
    } ?>        
    }
    
    .logo img { <?php
    if( of_get_option( 'logo_max_height' ) && of_get_option( 'logo_max_height' ) != 'none' ) { ?>
        max-height: <?php echo of_get_option('logo_max_height'); ?>; <?php        
    } ?>        
    }
    
    .top-nav ul { <?php 
  	if( of_get_option( 'logo_position' ) ) {
    	$logo_position = of_get_option( 'logo_position' );
    
    	if( $logo_position == 'right' ) { ?>
			text-align: left; <?php			
		}
  	} ?>
    }
    
    .top-nav ul li { <?php 
  	if( of_get_option( 'logo_position' ) ) {
    	$logo_position = of_get_option( 'logo_position' );
    
    	if( $logo_position == 'right' ) { ?>
			margin: 0 15px 0 0; <?php			
		}
  	} ?>
    }
    
    .top-nav ul li a,
	.head-nav ul li a { <?php
    if( of_get_option( 'menu_font_color' ) ) { ?>
        color: <?php echo of_get_option('menu_font_color'); ?>; <?php        
    }
		
	if( of_get_option( 'menu_font_size' ) ) { ?>
        font-size: <?php echo of_get_option('menu_font_size'); ?>; <?php        
    } ?>
    }
    
    .top-nav ul li a { <?php
    if( of_get_option( 'top_menu_font' ) ) { 
        $font = of_get_option( 'top_menu_font' );
        $font_face = apply_filters('modify_options_font_face', $font['face']);
        $font_size = $font['size'];
        $font_color = $font['color'];
        $font_style = $font['style'];
        
        if($font_face) { ?>
        font-family: <?php echo $font_face; ?>; <?php            
        }
        
        if($font_size) { ?>
        font-size: <?php echo $font_size; ?>; <?php            
        }
        
        if($font_color) { ?>
        color: <?php echo $font_color; ?>; <?php            
        }
        
        if($font_style) {
            $font_style_component = explode(" ", $font_style);
            
            foreach($font_style_component as $value) {
                if($value == 'italic') { ?>
        font-style: <?php echo $value; ?>; <?php
                } elseif($value == 'normal' || $value == 'bold') { ?>
        font-weight: <?php echo $value; ?>; <?php                    
                }                
            }
        }
    }
    ?>
    }

	.head-nav ul li.current-menu-item > a { <?php    
    if( of_get_option( 'selected_menu_font_color' ) ) { ?>
        color: <?php echo of_get_option('selected_menu_font_color'); ?>; <?php        
    } ?>
    }
    
    .top-nav ul li ul { <?php
        $topnav_padding = ( of_get_option( 'top_header_padding' )? : '10px' );
        $topnav_padding_components = getPaddingComponents( $topnav_padding );
        
        $topnav_padding_bottom = $topnav_padding_components['bottom']; ?>
        top: calc(100% + <?php echo $topnav_padding_bottom; ?>); <?php
    ?>
    }
    
	.top-nav ul li ul li,
    .head-nav ul li ul li { <?php
    if( of_get_option( 'submenu_bg_color' ) ) { ?>
        background-color: <?php echo of_get_option('submenu_bg_color'); ?>; <?php        
    } ?>
    }

	.top-nav ul li ul li a,
	.head-nav ul li ul li a { <?php    
    if( of_get_option( 'submenu_font_color' ) ) { ?>
        color: <?php echo of_get_option('submenu_font_color'); ?>; <?php        
    }
		
	if( of_get_option( 'submenu_font_size' ) ) { ?>
        font-size: <?php echo of_get_option('submenu_font_size'); ?>; <?php        
    } elseif( of_get_option( 'menu_font_size' ) ) { ?>
        font-size: <?php echo of_get_option('menu_font_size'); ?>; <?php 		
	} ?>
    } 
    
    .head-nav ul li ul li.current-menu-item > a { <?php    
    if( of_get_option( 'selected_submenu_font_color' ) ) { ?>
        color: <?php echo of_get_option('selected_submenu_font_color'); ?>; <?php        
    } ?>
    }  	
	
	.slicknav_nav { <?php		
	if( of_get_option( 'submenu_bg_color' ) ) { ?>
        background-color: <?php echo of_get_option('submenu_bg_color'); ?>; <?php        
    } ?>		
	}
	
	.slicknav_menu ul li a { <?php 
	if( of_get_option( 'submenu_font_color' ) ) { ?>
        color: <?php echo of_get_option('submenu_font_color'); ?>; <?php        
    }
    
    if( of_get_option( 'submenu_font_size' ) ) { ?>
        font-size: <?php echo of_get_option('submenu_font_size'); ?>; <?php        
    } elseif( of_get_option( 'menu_font_size' ) ) { ?>
        font-size: <?php echo of_get_option('menu_font_size'); ?>; <?php 		
	} ?>		
	}

	.slicknav_nav .slicknav_row:hover,
	.slicknav_menu ul li a:hover a, 
    .slicknav_menu ul li a:hover, 
    .slicknav_menu ul li ul li a:hover, 
    .slicknav_nav .slicknav_row:hover { <?php 
    if( of_get_option( 'mobile_menu_highlight_color' ) ) { ?>
        background-color: <?php echo of_get_option('mobile_menu_highlight_color'); ?>; <?php        
    } ?>
    }
    
    .content { <?php	
    $content_margintop = of_get_option( 'content_margintop' );
	$mobile_content_margintop = of_get_option( 'mobile_content_margintop' );
    
    if( isset( $content_margintop ) && $content_margintop != '' ) { ?>
        margin-top: <?php echo of_get_option('content_margintop'); ?>; <?php        
    } ?>
    }
    
    footer#foot .main-foot { <?php
    if( of_get_option( 'footer_primary_bg_color' ) ) { ?>
        background-color: <?php echo of_get_option('footer_primary_bg_color'); ?>; <?php        
    }
    
    if( of_get_option( 'footer_primary_padding' ) ) { ?>
        padding: <?php echo of_get_option('footer_primary_padding'); ?>; <?php        
    }
    
    if( of_get_option( 'footer_primary_font' ) ) { 
        $font = of_get_option( 'footer_primary_font' );
        $font_face = apply_filters('modify_options_font_face', $font['face']);
        $font_size = $font['size'];
        $font_color = $font['color'];
        $font_style = $font['style'];
        
        if($font_face) { ?>
        font-family: <?php echo $font_face; ?>; <?php            
        }
        
        if($font_size) { ?>
        font-size: <?php echo $font_size; ?>; <?php            
        }
        
        if($font_color) { ?>
        color: <?php echo $font_color; ?>; <?php            
        }
        
        if($font_style) {
            $font_style_component = explode(" ", $font_style);
            
            foreach($font_style_component as $value) {
                if($value == 'italic') { ?>
        font-style: <?php echo $value; ?>; <?php
                } elseif($value == 'normal' || $value == 'bold') { ?>
        font-weight: <?php echo $value; ?>; <?php                    
                }                
            }
        }
    } ?>
    }
        
    footer#foot .main-foot h1,
    footer#foot .main-foot h2,
    footer#foot .main-foot h3,
    footer#foot .main-foot h4,
    footer#foot .main-foot h5,
    footer#foot .main-foot h6 {<?php    
    if( of_get_option( 'footer_primary_footer_font' ) ) {
        $font = of_get_option( 'footer_primary_footer_font' );
        $font_face = apply_filters('modify_options_font_face', $font['face']);
        $font_size = $font['size'];
        $font_color = $font['color'];
        $font_style = $font['style'];
        
        if($font_face) { ?>
        font-family: <?php echo $font_face; ?>; <?php            
        }
        
        if($font_size) { ?>
        font-size: <?php echo $font_size; ?>; <?php            
        }
        
        if($font_color) { ?>
        color: <?php echo $font_color; ?>; <?php            
        }
        
        if($font_style) {
            $font_style_component = explode(" ", $font_style);
            
            foreach($font_style_component as $value) {
                if($value == 'italic') { ?>
        font-style: <?php echo $value; ?>; <?php
                } elseif($value == 'normal' || $value == 'bold') { ?>
        font-weight: <?php echo $value; ?>; <?php                    
                }                
            }
        }
    } ?>
    }
    
    footer#foot .bottom-foot { <?php
    if( of_get_option( 'footer_secondary_bg_color' ) ) { ?>
        background-color: <?php echo of_get_option('footer_secondary_bg_color'); ?>; <?php        
    }
    
    if( of_get_option( 'footer_secondary_padding' ) ) { ?>
        padding: <?php echo of_get_option('footer_secondary_padding'); ?>; <?php        
    }
    
    if( of_get_option( 'footer_secondary_font' ) ) { 
        $font = of_get_option( 'footer_secondary_font' );
        $font_face = apply_filters('modify_options_font_face', $font['face']);
        $font_size = $font['size'];
        $font_color = $font['color'];
        $font_style = $font['style'];
        
        if($font_face) { ?>
        font-family: <?php echo $font_face; ?>; <?php            
        }
        
        if($font_size) { ?>
        font-size: <?php echo $font_size; ?>; <?php            
        }
        
        if($font_color) { ?>
        color: <?php echo $font_color; ?>; <?php            
        }
        
        if($font_style) {
            $font_style_component = explode(" ", $font_style);
            
            foreach($font_style_component as $value) {
                if($value == 'italic') { ?>
        font-style: <?php echo $value; ?>; <?php
                } elseif($value == 'normal' || $value == 'bold') { ?>
        font-weight: <?php echo $value; ?>; <?php                    
                }                
            }
        }
    } ?>
    }
    
    .foot-socials a { <?php 
    if( of_get_option( 'social_media_icon_size' ) ) { ?>
        font-size: <?php echo of_get_option( 'social_media_icon_size' ); ?>; <?php        
    } else { ?>
        font-size: 25px; <?php        
    } 
    
    if( of_get_option( 'social_media_icon_color' ) ) { ?>
        color: <?php echo of_get_option( 'social_media_icon_color' ); ?>; <?php        
    } ?>
    }
    
    .foot-socials a:hover { <?php     
    if( of_get_option( 'social_media_icon_hover_color' ) ) { ?>
        color: <?php echo of_get_option( 'social_media_icon_hover_color' ); ?>; <?php        
    } ?>
    }
    
    .ubicomp-form--container { <?php    
    if( of_get_option( 'ubicomp_bg_color' ) ) { ?>
        background-color: <?php echo hexToRgb( of_get_option( 'ubicomp_bg_color' ), of_get_option( 'ubicomp_bg_opacity' ) ); ?>; <?php        
    } 
      
    if( of_get_option( 'logo_position' ) ) {
    	$logo_position = of_get_option( 'logo_position' );
    
    	if( $logo_position == 'top' ) { ?>
      		top: 150px; <?php      
    	} 
  	} ?>        
    }
    
    .ubicomp-form--container label { <?php    
    if( of_get_option( 'ubicomp_label_color' ) ) { ?>
        color: <?php echo of_get_option( 'ubicomp_label_color' ); ?>; <?php        
    } ?>
    }
    
    .ubicomp-form--container .closebookbtn { <?php    
    if( of_get_option( 'ubicomp_label_color' ) ) { ?>
        color: <?php echo of_get_option( 'ubicomp_label_color' ); ?>; <?php        
    } ?>
    }
	
	.qikres-login-menu a > span.user-icon { <?php    
    if( of_get_option( 'guest_token_bg_color' ) ) { ?>
        background-color: <?php echo of_get_option( 'guest_token_bg_color' ); ?>; <?php        
    }
		
	if( of_get_option( 'guest_token_font_color' ) ) { ?>
        color: <?php echo of_get_option( 'guest_token_font_color' ); ?>; <?php        
    } ?>		
	}

	@media only screen and (max-width: 1200px) {
        .logo { <?php 
        if( of_get_option( 'logo_position' ) ) {
            $logo_position = of_get_option( 'logo_position' );

            if( $logo_position == 'top' ) { ?>
                width: 100%; <?php      
            } 
        } ?>
        }
	}

	@media only screen and (max-width: 960px) {
        .content { <?php 		
        if( isset( $mobile_content_margintop ) && $mobile_content_margintop != '' ) { ?>
        	margin-top: <?php echo of_get_option( 'mobile_content_margintop' ); ?>; <?php  
        } ?>
        }
	}

	@media only screen and (max-width: 767px) {
        .logo { <?php 
        if( of_get_option( 'logo_position' ) ) {
            $logo_position = of_get_option( 'logo_position' );

            if( $logo_position == 'top' ) { ?>
                width: 100%; <?php      
            } 
        } ?>
        }
	}
</style>