<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title( '|', true, 'right' ); ?></title>
        <?php wp_head(); ?>
        <?php require( get_stylesheet_directory() . '/ubicomp/ubicomp-script.js.php'); ?>
        <?php include( get_stylesheet_directory() . '/ubicomp/script.js.php'); ?>
        <?php
        if ( function_exists('of_get_option') ) {
            include( get_stylesheet_directory() . '/theme-options.css.php');
        }
        ?>
    </head>
    <body <?php body_class(); ?>>
        <?php include( get_stylesheet_directory() . '/ubicomp/booking-form.php'); ?>
        <header id="head">
            <div class="container">
                <div class="head-area">
                    <?php
                    if (has_nav_menu('top-nav-menu')) {
                        ?>
                        <div class="top-nav">
                            <?php wp_nav_menu(array('theme_location' => 'top-nav-menu')); ?>
                        </div>
                        <?php
                    }
                    ?>	
                    <div class="head-menu-nav">									
                        <div class="logo">
                            <?php echo (dess_setting('dess_logo') != '' ? '<a href="' . qikres_homepage() . '"><img src="' . dess_setting('dess_logo') . '" alt="logo" /></a>' : '<a href="' . qikres_homepage() . '"><img src="' . esc_url(get_stylesheet_directory_uri()) . '/images/logo.png" alt="logo" /></a>'); ?>	
                        </div><!-- logo -->
                        <div class="head-nav">
                            <?php wp_nav_menu(array('theme_location' => 'header-menu')); ?>
                        </div>					
                    </div>
                    <!-- head-nav -->
                </div><!-- head-area -->
            </div><!-- container -->
        </header>