 		<footer id="foot">
			<div class="main-foot">
				<div class="container">
					<div class="foot-col"><?php 
					if ( is_active_sidebar( 'footer-1' ) ) {
                      	dynamic_sidebar('footer-1');
                    } else {
                      	echo '&nbsp;';
                    } ?>
					</div>
					<div class="foot-col"><?php 
					if ( is_active_sidebar( 'footer-2' ) ) {
                      	dynamic_sidebar('footer-2');
                    } else {
                      	echo '&nbsp;';
                    } ?>
					</div>
					<div class="foot-col"><?php 
					if ( is_active_sidebar( 'footer-3' ) ) {
                      	dynamic_sidebar('footer-3');
                    } else {
                      	echo '&nbsp;';
                    } ?>
					</div>					
				</div>
			</div>
			<div class="bottom-foot">
				<div class="container">
					<div class="foot-socials">
						<ul>
							<?php
								$socials = array('twitter','facebook','google-plus','instagram','pinterest','vimeo','youtube','linkedin');
								for($i=0;$i<count($socials);$i++){
									$url = '';
									$s = $socials[$i];
									$url = dess_setting('dess_'.$s);
									echo ($url != '' ? '<li><a target="_blank" href="'.$url.'"><i class="fa fa-fw fa-'.$s.'"></i></a></li>':'');
								}
							?>
						</ul>
					</div>
					<div class="copyright">
						<p class="credits">
							<?php
							if ( function_exists( 'pll__' ) ) :
								echo (dess_setting('dess_copyright') !='' ? pll__( dess_setting('dess_copyright') ) : '2016 Copyright. Powered by WordPress');
							else:
								echo (dess_setting('dess_copyright') !='' ? dess_setting('dess_copyright') : '2016 Copyright. Powered by WordPress');
							endif;
							?>
						</p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>